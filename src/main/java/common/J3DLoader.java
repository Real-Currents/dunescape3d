package common;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Write a description of class J3DLoader here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class J3DLoader {

    /**
     * Constructor for objects of class J3DLoader
     */
    public J3DLoader() {
        System.setProperty(
            "java.library.path",
            Paths.get("lib/Java3D/1.5.2/bin").toString()
        );
        
        System.out.println("Library path is "+ System.getProperty("java.library.path").toString());
        
        //System.loadLibrary("j3dcore-d3d");
        System.loadLibrary("j3dcore-ogl");
        //System.loadLibrary("j3dcore-ogl-cg");
        //System.loadLibrary("j3dcore-ogl-chk");
    }
}
