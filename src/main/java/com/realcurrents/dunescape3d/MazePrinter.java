package com.realcurrents.dunescape3d;

import java.util.ArrayList;
import static java.lang.Math.random;
import static java.lang.System.out;

/**
 *
 * @author <john@real-currents.com>
 */
public class MazePrinter {
    public static int dimension = 32;
    
    /****************************************************************/
    public static void main(String[] args) {
        MazeChars mc = new MazeChars(args);
        if (mc.isEmpty()) mc.<String>add("x2571");
        while (mc.size() < (dimension*dimension/2) + 1) {
            mc.makeMaze(utfChar -> {
                out.print((char)((double)utfChar + 0.5 + random()));
                return mc.doubleToChar(((double)utfChar + 0.5 + random()));
            });
        }
    }
    /****************************************************************/

    interface MazeMaker<T> {
        public String make (int utfChar);
    }
    
    static class MazeChars<T extends String> extends ArrayList<String> {
        
        public MazeChars (String[] arr) {
            super();
            for (String elt: arr) this.add(elt);
        }
        
        public String doubleToChar (Double dChar) {
            return ((Integer)(new Double(dChar).intValue())).toString();
        }
        
        public String hexToChar (String hChar) {
            return (Integer.valueOf(hChar.substring(1), 16)).toString();
        }
        
        public boolean add (String elt) {
            return super.<String>add(elt);
        }
        
        public void makeMaze (MazeMaker mm) {
            double nextc = 0;
            int charRange = ((int)(this.size() - 1)/MazePrinter.dimension)*MazePrinter.dimension;
            
            for (String utfChar : this.subList(charRange, charRange + ((this.size() - 1)%MazePrinter.dimension + 1))) {
                if (utfChar.toString().indexOf("x") == 0) nextc = (double) new Integer(hexToChar(utfChar.toString()));
                else nextc = (double) new Integer(utfChar.toString());
                if ((this.size()%MazePrinter.dimension) < 1) mm.make((int)nextc);
            }
            
            if (nextc != 0) this.<String>add(this.doubleToChar(nextc));
            
            if ((this.size() - 1)%MazePrinter.dimension < 1) out.print(this.size() - 1 +"\n");
        }
    }
}
